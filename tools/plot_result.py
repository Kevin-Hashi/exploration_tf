import matplotlib.pyplot as plt


class plot:
    model_name = ''
    model_dir_num = ''

    def __init__(self, model_name, model_dir_num, batch_size):
        self.model_name = model_name
        self.model_dir_num = model_dir_num
        self.batch_size=batch_size

    def plot_result(self, history):

        # accuracy
        plt.figure()
        plt.plot(history.history['acc'], label='acc', marker='.')
        plt.plot(history.history['val_acc'], label='val_acc', marker='.')
        plt.grid()
        plt.legend(loc='best')
        plt.title('accuracy')
        fig_name = self.model_dir_num + \
            self.model_name + '_accuracy_'+f'_{self.batch_size}.png'
        plt.savefig(fig_name)
        plt.close()

        # loss
        plt.figure()
        plt.plot(history.history['loss'], label='loss', marker='.')
        plt.plot(history.history['val_loss'], label='val_loss', marker='.')
        plt.grid()
        plt.legend(loc='best')
        plt.title('loss')
        fig_name = self.model_dir_num + \
            self.model_name + '_loss_'+f'_{self.batch_size}.png'
        plt.savefig(fig_name)
        plt.close()

    def save_result(self, acc_score, loss_score, batch_size, sample_size):
        with open(self.model_dir_num+'result.txt', mode='a') as f:
            f.write('\n'+str(batch_size)+' ' +
                    str(acc_score)+' '+str(loss_score)+' '+str(sample_size)+'\n')

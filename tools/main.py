import os
import numpy as np
import tensorflow as tf
from tensorflow.keras import callbacks
from tensorflow.keras.utils import plot_model
from . import train_data, callback, plot_result


def main(model_con, create_model_layers, input_shape, num_classes, epochs, batch_size, model_dir_num, es=True, da=False, resize_data=False, num_data=None, learn_late=1e-3):
    pr = plot_result.plot(model_con, model_dir_num)
    for batch_size_subdivision in batch_size:
        X_train, Y_train, x_test, y_test = train_data.cifar10(num_data)

        if es:
            # EarlyStopping
            early_stopping = callbacks.EarlyStopping(
                monitor='val_loss',
                patience=10,
                verbose=1
            )
        tensorboard = callbacks.TensorBoard(
            model_dir_num+'logs/', histogram_freq=1)
        modelcheckpoint = callbacks.ModelCheckpoint(model_dir_num+'checkpoint_{epoch}.h5')
        checkpoint=callback.checkpoint(model_dir_num)

        callBack = [tensorboard, modelcheckpoint, checkpoint]
        if es:
            callBack+=[early_stopping]

        # 学習モデル作成
        model = create_model_layers(num_classes, input_shape=input_shape)

        # 最適化アルゴリズム
        optimizer = tf.keras.optimizers.Adam(learn_late)

        # モデルの設定
        model.compile(
            loss='categorical_crossentropy',  # 損失関数の設定
            optimizer=optimizer,  # 最適化法の指定
            metrics=['acc'])

        # モデル情報表示
        model.summary()
        with open(model_dir_num+model_con+"_model_summary.txt", "w") as fp:
            model.summary(print_fn=lambda x: fp.write(x + "\n"))
        model_png = model_dir_num+model_con+f'_{batch_size_subdivision}.png'

        if os.path.isfile(model_png):
            os.remove(model_png)
        plot_model(model, to_file=model_png, show_shapes=True,
                   dpi=256, expand_nested=True)

        if resize_data:
            X_train, x_test = resize_gen(X_train, x_test, input_shape)
        gen_train = da_generator(X_train, Y_train, batch_size_subdivision) if da else generator(
            X_train, Y_train, batch_size_subdivision)

        # モデルの学習
        history = model.fit(
            gen_train,
            callbacks=callBack,
            steps_per_epoch=X_train.shape[0] /
            batch_size_subdivision if not da else gen_train.n/batch_size_subdivision,
            epochs=epochs,
            validation_data=(x_test, y_test),  # gen_val
            validation_steps=1,
            batch_size=batch_size_subdivision)

        # モデル保存
        model_name = model_dir_num+'model_' + \
            model_con + f'_{batch_size_subdivision}_{epochs}.h5'
        model.save(model_name)

        # 学習結果グラフ出力
        pr.plot_result(history, batch_size_subdivision)

        # 評価 & 評価結果出力
        score = model.evaluate(x_test, y_test)
        print()
        print('Test loss:', score[0])
        print('Test accuracy:', score[1])
        with open(model_dir_num+'result.txt', mode='a') as f:
            f.write('\n'+str(batch_size_subdivision)+' ' +
                    str(score[0])+' '+str(score[1])+' '+str(X_train.shape[0] if not da else gen_train.n)+'\n')


def da_generator(x_train, y_train, batch_size):
    return tf.keras.preprocessing.image.ImageDataGenerator(rotation_range=20, horizontal_flip=True, height_shift_range=0.2,
                                                           width_shift_range=0.2, zoom_range=0.2, channel_shift_range=0.2
                                                           ).flow(x_train, y_train, batch_size)


def generator(x_train, y_train, batch_size):
    return tf.keras.preprocessing.image.ImageDataGenerator().flow(x_train, y_train, batch_size)


def resize_gen(X_train, X_test, inputshape):
    import cv2
    import gc
    input_size = inputshape[0]
    num = len(X_train)
    zeros = np.zeros((num, input_size, input_size, 3))
    for i, img in enumerate(X_train):
        zeros[i] = cv2.resize(
            img,
            dsize=(input_size, input_size)
        )
    X_train = zeros
    del zeros
    X_train.shape
    gc.collect()
    num = len(X_test)
    zeros = np.zeros((num, input_size, input_size, 3))
    for i, img in enumerate(X_test):
        zeros[i] = cv2.resize(
            img,
            dsize=(input_size, input_size)
        )
    X_test = zeros
    del zeros
    X_test.shape
    gc.collect()
    # データの前処理
    X_train = np.array(X_train, np.float32) / 255
    X_test = np.array(X_test, np.float32) / 255

    return X_train, X_test

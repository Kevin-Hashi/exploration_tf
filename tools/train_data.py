import tensorflow as tf
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.utils import to_categorical
import numpy as np


def cinic10(batch_size, input_shape=(32, 32, 3), data_folder="C:\\Users\\hashi\\cinic-10\\", shuffle=True):
    idg_train = ImageDataGenerator(
        rescale=1.0/255.0,
        # ↑学習と推論で同じスケールにする必要がある。
        # ↑転移学習の場合はベースモデルのスケールと合わせる必要がある。

    )
    idg_validation = ImageDataGenerator(
        rescale=1.0/255.0
    )
    gen_train = idg_train.flow_from_directory(
        directory=data_folder+"train",
        target_size=input_shape[:2],
        batch_size=batch_size,
        class_mode='categorical',
        shuffle=shuffle
    )
    gen_validation = idg_validation.flow_from_directory(
        directory=data_folder+"valid",
        target_size=input_shape[:2],
        batch_size=batch_size,
        class_mode='categorical',
        shuffle=shuffle
    )
    return gen_train, gen_validation

def cifar10(num_data):
    (X_train,Y_train),(x_test,y_test)=tf.keras.datasets.cifar10.load_data()
    if num_data!=None:
        X_train=X_train[:num_data]
        Y_train=Y_train[:num_data]
    X_train=np.array(X_train,np.float32)/255
    x_test=np.array(x_test,np.float32)/255
    Y_train=to_categorical(Y_train,10)
    y_test=to_categorical(y_test,10)

    return X_train,Y_train,x_test,y_test
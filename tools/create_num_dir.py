import os
def model_num(model_dir):
    os.makedirs(model_dir,exist_ok=True)
    files_dir = [f for f in os.listdir(model_dir) if os.path.isdir(os.path.join(model_dir, f))]
    print("dir: "+" ".join(files_dir))
    while 1:
        model_num = input("model_num : ")
        if os.path.isdir(model_dir+str(model_num)):
            t_f = input("ディレクトリが存在しますが上書きしてよろしいですか？(y/n)")
            if t_f.lower() in {'y', 'yes'}:
                break
            else:
                pass
        else:
            os.makedirs(model_dir+str(model_num))
            break
    return model_num
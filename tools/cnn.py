from tensorflow.keras.layers import Conv2D, BatchNormalization, MaxPooling2D, Dense, Dropout, GlobalAveragePooling2D
from tensorflow.keras.regularizers import l2
class cnn_tools:
    def __init__(self,l2_late=1e-5) -> None:
        self.l2_late=l2_late
    def conv2d(self,filters, x):
        return Conv2D(filters, 3, padding='same', activation='relu',
                    kernel_regularizer=l2(self.l2_late))(x)


    def dense_relu(self,filters, x):
        return Dense(filters, activation='relu', kernel_regularizer=l2(self.l2_late))(x)


def bn(x):
    return BatchNormalization()(x)


def m_pool2d(x):
    return MaxPooling2D()(x)


def dp(rate, x):
    return Dropout(rate)(x)


def GA_pool(x):
    return GlobalAveragePooling2D()(x)
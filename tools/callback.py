import os
import shutil

from tensorflow.keras.callbacks import Callback

from . import plot_result


class checkpoint(Callback):
    def __init__(self, model_dir_num, name):
        super().__init__()
        self.model_dir_num = model_dir_num
        self.val_loss = 10000
        self.val_loss_best = 10000
        self.val_acc = 0
        self.val_acc_best = 0
        self.make_acc_model = 0
        self.make_loss_model = 0
        self.epoch=0
        self.name=name

    def on_epoch_end(self, epoch, logs=None):

        self.epoch=epoch

        if epoch == 0:
            pass
        else:
            modelname=self.model_dir_num+f'{self.name}{epoch}.h5'
            if os.path.isfile(modelname):
                print(f'find & del {modelname}')
                os.remove(modelname)
            else:
                print(f'Not find {modelname}')

        self.val_acc = logs.get('val_acc')
        self.val_loss = logs.get('val_loss')

        if epoch == 0:
            pass
        else:
            if self.val_acc >= self.val_acc_best:
                self.val_acc_best = self.val_acc
                accname=self.model_dir_num+f'{self.name}val_acc_{self.make_acc_model}.h5'
                if os.path.isfile(accname):
                    print(
                        f'find & del {accname}')
                    os.remove(accname)

                self.make_acc_model = epoch+1
                shutil.copy2(
                    self.model_dir_num+f'{self.name}{epoch+1}.h5', self.model_dir_num+f'{self.name}val_acc_{epoch+1}.h5')

            if self.val_loss <= self.val_loss_best:
                self.val_loss_best = self.val_loss
                lossname=self.model_dir_num+f'{self.name}val_loss_{self.make_loss_model}.h5'
                if os.path.isfile(lossname):
                    print(
                        f'find & del {lossname}')
                    os.remove(lossname)

                self.make_loss_model = epoch+1
                shutil.copy2(
                    self.model_dir_num+f'{self.name}{epoch+1}.h5', self.model_dir_num+f'{self.name}val_loss_{epoch+1}.h5')

    def on_train_end(self, logs=None):
        modelname=self.model_dir_num+f'{self.name}{self.epoch+1}.h5'
        if os.path.isfile(modelname):
            print('find & del '+modelname)
            os.remove(modelname)
        else:
            print('Not find '+modelname)

class saveTrainImage(Callback):
    def __init__(self, model_name, model_dir_num, batch_size) -> None:
        self.history=history()
        self.pr=plot_result.plot(model_name,model_dir_num,batch_size)
    def on_epoch_end(self, epoch, logs=None):
        self.history.append_value('acc',logs.get('acc'))
        self.history.append_value('val_acc',logs.get('val_acc'))
        self.history.append_value('loss',logs.get('loss'))
        self.history.append_value('val_loss',logs.get('val_loss'))
        self.pr.plot_result(self.history)

class history:
    def __init__(self) -> None:
        self.history={'acc':[],'val_acc':[],'loss':[],'val_loss':[]}
    def append_value(self, name,val):
        self.history[name].append(val)
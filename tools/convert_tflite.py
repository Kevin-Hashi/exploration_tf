import os
import tensorflow as tf
def convert(model_name, save_file):
    model=tf.keras.models.load_model(model_name)
    converter=tf.lite.TFLiteConverter.from_keras_model(model)
    tflite_model=converter.convert()
    open(save_file,"wb").write(tflite_model)
if __name__=="__main__":
    model_name=input('modelname:')
    save_file=model_name.replace(".h5",".tflite")
    convert(model_name,save_file)

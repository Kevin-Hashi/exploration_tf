import os

import numpy as np
import tensorflow as tf
from tensorflow.keras import callbacks
from tensorflow.keras.layers import Concatenate, Dense, Input
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.regularizers import l2
from tensorflow.keras.utils import plot_model

from tools import callback, create_num_dir, main
from tools.cnn import *


def block1(x, rate=1):
    s2 = x
    s4 = x

    s2 = conv2d(128*rate, s2)
    s2 = bn(s2)
    s2 = dp(0.35, s2)

    s4 = conv2d(256*rate, s4)
    s4 = bn(s4)
    s4 = dp(0.25*rate, s4)
    s4 = m_pool2d(s4)

    s2 = conv2d(128*rate, s2)
    s2 = m_pool2d(s2)
    s = Concatenate()([s2, s4])

    del s2, s4
    return s


def block2(x, rate=1):
    s = x
    x = conv2d(128*rate, x)
    x = bn(x)
    x = dp(0.35, x)
    s2 = dp(0.25, x)
    x = conv2d(128*rate, x)
    x = bn(x)
    x = dp(0.35, x)

    x = Concatenate()([x, s, s2])

    del s, s2
    return x


def create_model_layers(num_classes, input_shape):
    base_model = tf.keras.applications.MobileNetV2(
        input_shape=input_shape,
        include_top=False,
        weights="imagenet"
    )
    layer_names = [l.name for l in base_model.layers]
    idx = layer_names.index('block_12_expand')
    base_model.trainable = True

    for layer in base_model.layers[:idx]:
        layer.trainable = False
    x = base_model.output
    x = conv2d(64, x)
    s1 = block1(x)
    s2 = block2(x)
    s2 = conv2d(128, s2)
    s2 = m_pool2d(s2)

    x = Concatenate()([s1, s2])

    x = conv2d(512, x)
    x = bn(x)
    x = dp(0.4, x)

    s1 = block1(x, 2)
    s2 = block2(x, 2)
    s2 = conv2d(128, s2)
    s2 = m_pool2d(s2)

    x = Concatenate()([s1, s2])

    x = conv2d(1024, x)
    x = bn(x)
    x = dp(0.4, x)

    x = GA_pool(x)

    x = denseRelu(1024, x)
    x = dp(0.5, x)

    x = Dense(num_classes, activation='softmax')(x)
    return Model(base_model.input, x)


def main():
    gen_train, gen_test = train_data()
    tensorboard = callbacks.TensorBoard(
        model_dir_num+'logs/', histogram_freq=1)
    modelcheckpoint = callbacks.ModelCheckpoint(
        model_dir_num+'checkpoint_{epoch}.h5')
    checkpoint = callback.checkpoint(model_dir_num, 'checkpoint_')
    save_train_image = callback.saveTrainImage(
        model_name, model_dir_num, batch_size)
    callBackList = [tensorboard, save_train_image, modelcheckpoint, checkpoint]

    model = create_model_layers(num_classes, input_shape)
    optimizer = Adam(learn_rate)

    model.compile(loss='categorical_crossentropy',
                  optimizer=optimizer, metrics=['acc'])

    model.summary()
    # モデル情報表示
    model.summary()
    with open(model_dir_num+model_name+"_model_summary.txt", "w") as fp:
        model.summary(print_fn=lambda x: fp.write(x + "\n"))
    model_png = model_dir_num+model_name+f'_{batch_size}.png'

    if os.path.isfile(model_png):
        os.remove(model_png)
    plot_model(model, to_file=model_png, show_shapes=True,
               dpi=400, expand_nested=True)

    model.fit(gen_train, callbacks=callBackList, steps_per_epoch=gen_train.n//batch_size,
              epochs=epochs, validation_data=gen_test, validation_steps=1, batch_size=batch_size)

    save_name = model_dir_num+'model_'+model_name+f'_{batch_size}_{epochs}.h5'
    model.save(save_name)


def train_data():
    data_folder = "test_train/"
    idg_train = ImageDataGenerator(
        rescale=1.0/255.0,
        rotation_range=30,
        zoom_range=0.1,
    )
    idg_validation = ImageDataGenerator(
        rescale=1.0/255.0
    )
    gen_train = idg_train.flow_from_directory(
        directory=data_folder+"train",
        target_size=input_shape[:2],
        batch_size=batch_size,
        class_mode='categorical',
    )
    gen_validation = idg_validation.flow_from_directory(
        directory=data_folder+"test",
        target_size=input_shape[:2],
        batch_size=batch_size,
        class_mode='categorical',
        shuffle=False
    )
    return gen_train, gen_validation


if __name__ == "__main__":
    model_name = os.path.splitext(os.path.basename(__file__))[0]
    model_dir = model_name+'/'

    model_num = create_num_dir.model_num(model_dir)

    model_dir_num = model_dir+model_num+'/'

    input_shape = (300, 300, 3)
    num_classes = 3
    epochs = 50
    batch_size = 15
    learn_rate = 1e-4

    Layer = cnn_tools()
    conv2d = Layer.conv2d
    denseRelu = Layer.dense_relu

    try:
        main()
    except KeyboardInterrupt:
        pass

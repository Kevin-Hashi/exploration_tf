package jp.gr.java_conf.hashiguchi_homemade.ex_ai_jenken

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.ImageFormat
import android.graphics.SurfaceTexture
import android.hardware.camera2.*
import android.media.ImageReader
import android.os.*
import android.util.Log
import android.view.Surface
import android.view.TextureView
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import java.io.*
import java.util.*

@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity() {

    //judge the reason for leaving this app is Intent or finish
    private var inIntent=false

    // PermissionCode
    private val permissionCode=201

    // use to start Intent for choose picture
    private val choseFileCode = 12345

    // Display Picture selected in Intent
    private lateinit var imageView:ImageView

    // Get Picture by Camera (maybe...)
    private lateinit var imageReader: ImageReader

    // Display Camera
    private lateinit var previewView: TextureView

    // for Camera
    private lateinit var previewRequestBuilder: CaptureRequest.Builder
    private lateinit var previewRequest: CaptureRequest
    private lateinit var captureSession: CameraCaptureSession
    
    private var backgroundHandler: Handler?=null
    private var backgroundThread: HandlerThread?=null
    private var cameraDevice: CameraDevice?=null
    
    private val stateCallback=object:CameraDevice.StateCallback(){
        override fun onOpened(cameraDevice: CameraDevice){
            this@MainActivity.cameraDevice=cameraDevice
            createCameraPreviewSession()
        }
        override fun onDisconnected(cameraDevice: CameraDevice){
            imageReader.close()
            //onPause()
            this@MainActivity.cameraDevice=null
        }
        override fun onError(cameraDevice: CameraDevice,error:Int){
            onDisconnected(cameraDevice)
            finish()
        }
    }
    private val surfaceTextureListener=object : TextureView.SurfaceTextureListener{
        override fun onSurfaceTextureAvailable(surface:SurfaceTexture,width:Int,height:Int) {
            imageReader= ImageReader.newInstance(width,height,ImageFormat.JPEG,2)
            openCamera()
        }

        override fun onSurfaceTextureSizeChanged(p0: SurfaceTexture, p1: Int, p2: Int) {}
        override fun onSurfaceTextureUpdated(p0: SurfaceTexture) {}
        override fun onSurfaceTextureDestroyed(p0: SurfaceTexture): Boolean {
            return false
        }
    }

    //Classifier Values
    private lateinit var classifier:Classifier
    private var loadModelBefore=false
    private val modelName="model_train_15_50.tflite"
    private val classLabel= listOf("グー","チョキ","パー")


    // Permission request
    private fun requestPermission(): List<Boolean> {
        requestPermissions(
            arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.CAMERA),
            permissionCode
        )
        return listOf(checkSelfPermission(Manifest.permission.CAMERA)==PackageManager.PERMISSION_GRANTED,checkSelfPermission(Manifest.permission.CAMERA)==PackageManager.PERMISSION_GRANTED)
    }
    
    // function for Camera
    private fun startBackgroundThread(){
        backgroundThread=HandlerThread("CameraBackground").also { it.start() }
        backgroundHandler= Handler(backgroundThread?.looper!!)
    }
    private fun createCameraPreviewSession(){
        try {
            val texture=previewView.surfaceTexture
            texture!!.setDefaultBufferSize(previewView.width,previewView.height)
            val surface=Surface(texture)
            previewRequestBuilder=cameraDevice!!.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW)
            previewRequestBuilder.addTarget(surface)

            cameraDevice?.createCaptureSession(
                listOf(surface,imageReader.surface),
            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            object :CameraCaptureSession.StateCallback(){
                override fun onConfigured(cameraCaptureSession: CameraCaptureSession){
                    if (cameraDevice==null) return
                    try {
                        captureSession=cameraCaptureSession
                        previewRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE,CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE)
                        previewRequest=previewRequestBuilder.build()
                        cameraCaptureSession.setRepeatingRequest(previewRequest, null, Handler(backgroundThread?.looper!!))
                    }catch (e:CameraAccessException){
                        Log.e("erfs",e.toString())
                    }
                }

                override fun onConfigureFailed(session: CameraCaptureSession) {

                }
            },null)

        } catch (e:CameraAccessException){
            Log.e("erf",e.toString())
        }
    }
    private fun openCamera(){
        val manager:CameraManager=getSystemService(Context.CAMERA_SERVICE) as CameraManager
        try {
            val cameraId:String=manager.cameraIdList[0]
            val permission=ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
            if (permission!=PackageManager.PERMISSION_GRANTED){
                Log.e("openCamera","DENIED CAMERA")
                if (!requestPermission()[1]) return
                Log.e("openCamera","DENIED CAMERA AGAIN")
            }
            manager.openCamera(cameraId,stateCallback, null)
        }catch (e:Exception){
            e.printStackTrace()
        }
    }
    private fun releaseCamera(){
        if(::captureSession.isInitialized){
            captureSession.close()
        }
        cameraDevice?.close()

        if(::imageReader.isInitialized){
            imageReader.close()
        }
    }

    // get Classify result
    private fun resultClassify(bitmap: Bitmap){
        if(!loadModelBefore){
            classifier=Classifier(this,modelName)
            loadModelBefore=true
        }
        val resultTextView:TextView=findViewById(R.id.resultText)
        val label=classifier.classifyImageFromBitmap(bitmap)
        resultTextView.text=if(label!=-1){classLabel[label]}else{"判定できませんでした……"}
        Log.i("Classify Label", "Label ${classLabel[label]}")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startBackgroundThread()
        setContentView(R.layout.activity_main)

        val button: Button = findViewById(R.id.button)
        val cameraOrFile:SwitchCompat=findViewById(R.id.camera_or_file)
        previewView=findViewById(R.id.texture)
        imageView=findViewById(R.id.image)

        imageView.isVisible=true
        previewView.isInvisible=true

        var cameraPermission=checkCallingOrSelfPermission(Manifest.permission.CAMERA)!=PackageManager.PERMISSION_GRANTED
        var readExStoragePermission=checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)==PackageManager.PERMISSION_GRANTED

        if(checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)+checkCallingOrSelfPermission(Manifest.permission.CAMERA)!=PackageManager.PERMISSION_GRANTED){
            requestPermission()
            val returnPermission=requestPermission()
            cameraPermission=returnPermission[0]
            readExStoragePermission=returnPermission[1]
        }

        if (readExStoragePermission) {
            button.isClickable = true
        }else{
            Log.d("Test", "Doesn't have READ_EXTERNAL_STORAGE permission")
            Toast.makeText(this, "データへのアクセス権限が許可されていません", Toast.LENGTH_LONG).show()
            button.isClickable = false
        }

        previewView.surfaceTextureListener = surfaceTextureListener

        cameraOrFile.setOnCheckedChangeListener {
                _,isChecked-> Log.i("switch toggle","switch $isChecked")
            if (isChecked){
                if (!cameraPermission){
                    inIntent=true
                    cameraPermission=requestPermission()[1]
                }
                if (cameraPermission) {
                    imageView.isInvisible = true
                    previewView.isVisible = true
                    Log.i("imageView","Back")
                }
            }else{
                imageView.isVisible=true
                previewView.isInvisible=true
                Log.i("imageView","Front")
            }
        }
        button.setOnClickListener {
            if (cameraOrFile.isChecked){
                if (previewView.isAvailable){
                    captureSession.stopRepeating()
                    val bitmap:Bitmap= previewView.bitmap!!
                    val bmpWidth=bitmap.width
                    val bmpHeight=bitmap.height
                    Log.i("width",bmpWidth.toString())
                    Log.i("height",bmpHeight.toString())
                    resultClassify(bitmap)
                    captureSession.setRepeatingRequest(previewRequest, null, null)
                }
            }else{
                val intent = Intent(Intent.ACTION_GET_CONTENT)
                intent.type = "image/*"
                intent.action=Intent.ACTION_OPEN_DOCUMENT
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE,false)
                inIntent=true
                startActivityForResult(intent, choseFileCode)
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isEmpty()) {
            return
        }else{
            inIntent=false
        }
        when (requestCode) {
            permissionCode->{
                Log.d("onPermissionResult","permissionCode")
                val button:Button=findViewById(R.id.button)
                Log.d("permission array0",(grantResults[0]==PackageManager.PERMISSION_GRANTED).toString())
                button.isClickable=grantResults[0]==PackageManager.PERMISSION_GRANTED
                if (grantResults[0]==PackageManager.PERMISSION_DENIED){
                    button.setBackgroundColor(Color.rgb(150, 150, 150))
                }
                Log.d("permission array1",(grantResults[1]==PackageManager.PERMISSION_GRANTED).toString())
                val switchCompat:SwitchCompat=findViewById(R.id.camera_or_file)
                switchCompat.isClickable=grantResults[1]==PackageManager.PERMISSION_GRANTED
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (requestCode == choseFileCode && resultCode == RESULT_OK) {
                inIntent=false
                val filePathChange:String=getPathFromUri(this, data?.data!!).toString()
                val bitmap:Bitmap= getBitmapFromPath(filePathChange)
                imageView.setImageBitmap(bitmap)
                Log.i("filePath_change",filePathChange)
                Log.i("width",bitmap.width.toString())
                Log.i("height",bitmap.height.toString())
                resultClassify(bitmap)
            }

        } catch (e: UnsupportedEncodingException) {
            // いい感じに例外処理
            // 誰か思いついたら書いて……
            Log.e("onActivityResult",e.toString())
        }
    }

    override fun onResume() {
        super.onResume()
        Log.i("Activity","onResume")
        Log.i("onResume inIntent",inIntent.toString())
    }

    override fun onUserLeaveHint() {
        super.onUserLeaveHint()
        Log.e("CALLED","onUserLeaveHint")
        if (!inIntent){
            Log.e("FINISH","onUserLeaveHint")
            releaseCamera()
            finish()
        }
    }
}

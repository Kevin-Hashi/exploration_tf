package jp.gr.java_conf.hashiguchi_homemade.ex_ai_jenken

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.util.Log
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStream
import java.lang.Exception

fun getBitmapFromPath(filePath:String):Bitmap{
    var exifInterface: ExifInterface? =null
    try {
        exifInterface=ExifInterface(filePath)
    }catch (e:IOException){
        e.printStackTrace()
    }
    val orientation:Int= exifInterface!!.getAttributeInt(ExifInterface.TAG_ORIENTATION,ExifInterface.ORIENTATION_NORMAL)
    val bitmap:Bitmap= getRawBitmapFromPath(filePath)
    return when(orientation){
        ExifInterface.ORIENTATION_ROTATE_90->{ rotateImage(bitmap,90) }
        ExifInterface.ORIENTATION_ROTATE_180->{ rotateImage(bitmap,180) }
        ExifInterface.ORIENTATION_ROTATE_270->{ rotateImage(bitmap,270) }
        else->{ bitmap }
    }
}
fun getRawBitmapFromPath(pathString: String): Bitmap {
    val file= File(pathString)
    if(!file.exists()){
        Log.e("getRawBitmapFromPath","FILE DONT EXISTS")
        throw Exception("fail to load file")
    }
    val inputStream: InputStream = FileInputStream(pathString)
    return BitmapFactory.decodeStream(inputStream)
}
fun rotateImage(bitmap: Bitmap,degree:Int):Bitmap{
    val matrix= Matrix()
    matrix.postRotate(degree.toFloat())
    val rotatedImg:Bitmap= Bitmap.createBitmap(bitmap,0,0,bitmap.width,bitmap.height,matrix,true)
    bitmap.recycle()
    return rotatedImg
}
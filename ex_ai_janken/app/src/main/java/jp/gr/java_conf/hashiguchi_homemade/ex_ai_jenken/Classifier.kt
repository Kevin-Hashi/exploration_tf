package jp.gr.java_conf.hashiguchi_homemade.ex_ai_jenken

import android.app.Activity
import android.graphics.Bitmap
import org.tensorflow.lite.Interpreter
import java.io.FileInputStream
import java.io.IOException
import java.nio.ByteOrder
import java.nio.ByteBuffer
import java.nio.MappedByteBuffer
import java.nio.channels.FileChannel
import android.util.Log

@Suppress("DEPRECATION")
class Classifier(activity: Activity,model_name:String) {
    private val modelName=model_name
    private val imageSize:Int=300
    private val imageMean:Int=128
    private val imageSTD:Float=128.0f
    private val numClass:Int=3
    private val tfInterpreter:Interpreter=Interpreter(loadModelFile(activity))
    private val labelProbArray:Array<FloatArray> = Array(1){ FloatArray(numClass) }

    @Throws(IOException::class)
    private fun loadModelFile(activity: Activity):MappedByteBuffer{
        val fileDescriptor=activity.assets.openFd(modelName)
        Log.i("TF","MODEL LOADED")
        val inputStream=FileInputStream(fileDescriptor.fileDescriptor)
        val fileChannel=inputStream.channel
        val startOffset=fileDescriptor.startOffset
        val declaredLength=fileDescriptor.declaredLength
        return fileChannel.map(FileChannel.MapMode.READ_ONLY,startOffset,declaredLength)
    }

    fun classifyImageFromBitmap(bitmap: Bitmap):Int{
        val byteBuffer:ByteBuffer =
            if (bitmap.width!=imageSize||bitmap.height!=imageSize){
                val scaledBitmap=Bitmap.createScaledBitmap(bitmap,imageSize,imageSize,true)
                convertBitmapToByteBuffer(scaledBitmap)
            }else {
                convertBitmapToByteBuffer(bitmap)
            }

        val predict = classifyImage(byteBuffer)

        return oneHotLabel(predict[0])
    }

    private fun convertBitmapToByteBuffer(bitmap: Bitmap):ByteBuffer{
        val byteBuffer=ByteBuffer.allocateDirect(imageSize*imageSize*3*4)
        byteBuffer.order(ByteOrder.nativeOrder())
        val intValue=IntArray(imageSize*imageSize)

        bitmap.getPixels(intValue, 0,bitmap.width,0,0,bitmap.width,bitmap.height)
        var pixel=0
        for(i in 0 until imageSize){
            for(j in 0 until imageSize){
                val v=intValue[pixel++]

                byteBuffer.putFloat(((v.shr(16)and 0xFF)-imageMean)/imageSTD)
                byteBuffer.putFloat(((v.shr(8)and 0xFF)-imageMean)/imageSTD)
                byteBuffer.putFloat(((v and 0xFF)-imageMean)/imageSTD)
            }
        }
        return byteBuffer
    }

    private fun classifyImage(byteBuffer: ByteBuffer):Array<FloatArray>{
        tfInterpreter.run(byteBuffer,labelProbArray)
        return labelProbArray
    }

    private fun oneHotLabel(floatArray: FloatArray):Int{
        return floatArray.indices.maxBy { floatArray[it] }?:-1
    }
}
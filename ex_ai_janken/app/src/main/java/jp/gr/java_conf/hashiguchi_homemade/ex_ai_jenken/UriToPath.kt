package jp.gr.java_conf.hashiguchi_homemade.ex_ai_jenken

import android.content.ContentUris
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.util.Log

@Suppress("DEPRECATION")
fun getPathFromUri(context: Context, uri: Uri): String? {
    Log.i("uri","Uri:${uri.authority}")
    Log.i("scheme",uri.scheme.toString())
    Log.d("DocumentsContract.isDocumentUri(context,uri)",DocumentsContract.isDocumentUri(context,uri).toString())
    when {
        DocumentsContract.isDocumentUri(context,uri) -> {
            when (uri.authority) {
                "com.android.externalstorage.documents" -> {
                    val docId:String= DocumentsContract.getDocumentId(uri)
                    val split: List<String> =docId.split(":")
                    val type:String=split[0]
                    return if("primary".equals(type,ignoreCase = true)){
                        "${Environment.getExternalStorageDirectory()}/${split[1]}"
                    }else{
                        "/storage/$type/${split[1]}"
                    }
                }
                "com.android.providers.downloads.documents" -> {
                    val id:String= DocumentsContract.getDocumentId(uri)
                    val contentUri: Uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"),id.toLong()
                    )
                    return getDataColumn(context,contentUri, null,null)
                }
                "com.android.providers.media.documents" -> {
                    val docId:String= DocumentsContract.getDocumentId(uri)
                    val split:List<String> =docId.split(":")
                    val contentUri: Uri = MediaStore.Files.getContentUri("external")
                    val selection="_id=?"
                    val selectionArgs:Array<String> = arrayOf(split[1])
                    return getDataColumn(context,contentUri,selection,selectionArgs)
                }
            }
        }

        "content".equals(uri.scheme.toString(),ignoreCase = true) -> {
            Log.i("URI","equal content")
            return getDataColumn(context, uri, null, null)
        }
        "file".equals(uri.scheme.toString(),ignoreCase = true) -> {
            Log.i("URI","equal file")
            return uri.path.toString()
        }
    }
    return null
}

@Suppress("DEPRECATION")
fun getDataColumn(context: Context, uri: Uri, selection: String?, selectionArgs: Array<String>?): String? {
    var cursor: Cursor? =null
    val projection:Array<String> = arrayOf(MediaStore.Files.FileColumns.DATA)
    try{
        cursor= context.contentResolver.query(uri,projection,selection,selectionArgs,null)
        if (cursor!=null&&cursor.moveToFirst()){
            val cindex:Int=cursor.getColumnIndexOrThrow(projection[0])
            return cursor.getString(cindex)
        }
    }finally {
        cursor?.close()
    }
    return null
}

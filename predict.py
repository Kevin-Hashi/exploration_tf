import argparse
import glob
import os
import pathlib
import time

import numpy as np
import sklearn.metrics
import tensorflow as tf
from tensorflow.keras.preprocessing.image import ImageDataGenerator

parser = argparse.ArgumentParser(description='')
parser.add_argument('-a', '--all', action='store_true')
args = parser.parse_args()


def _main():
    result_file = open('predict.txt', mode='w')
    idg_validation = ImageDataGenerator(
        rescale=1.0/255.0
    )
    gen_validation = idg_validation.flow_from_directory(
        directory='test_train/'+"test",
        target_size=(300, 300),
        batch_size=5,
        class_mode='categorical',
        shuffle=False
    )
    # 学習済みのモデルをロード
    h5Files = glob.glob('./**/*.h5', recursive=True) if args.all else ['']
    h5Files.append(h5Files[0])
    for h5File in h5Files:
        print(h5File)
        start=time.perf_counter()
        model = tf.keras.models.load_model(h5File)
        preds = model.predict_generator(gen_validation)
        end=time.perf_counter()
        labels = ['0_gu', '1_tyoki', '2_pa']
        y_label = []
        for _ in range(sum(os.path.isfile(os.path.join('test_train/test/0_gu', name)) for name in os.listdir('test_train/test/0_gu'))):
            y_label.append('0_gu')
        for _ in range(sum(os.path.isfile(os.path.join('test_train/test/1_tyoki', name)) for name in os.listdir('test_train/test/1_tyoki'))):
            y_label.append('1_tyoki')
        for _ in range(sum(os.path.isfile(os.path.join('test_train/test/2_pa', name)) for name in os.listdir('test_train/test/2_pa'))):
            y_label.append('2_pa')

        # 推論ラベルの作成
        y_preds = []
        for p in preds:
            # 確信度が最大の値を推論結果とみなす
            label_idx = p.argmax()
            y_preds.append(labels[label_idx])

        # 混合行列を取得
        val_mat = sklearn.metrics.confusion_matrix(
            y_label, y_preds, labels=labels)
        print('[混合行列]')
        print(f'      {labels[0]: >6} {labels[1]: >6} {labels[2]: >6}')
        for i, row in enumerate(val_mat):
            print(
                f'{labels[i]: >7} {val_mat[i][0]: >4} {val_mat[i][1]: >6} {val_mat[i][2]: >6}')
        print(file=result_file)

        rec_score = sklearn.metrics.recall_score(
            y_label, y_preds, average=None)
        print('再現率： ', rec_score)

        pre_score = sklearn.metrics.precision_score(
            y_label, y_preds, average=None)
        print('適合率： ', pre_score)

        acc_score = sklearn.metrics.accuracy_score(y_label, y_preds)
        print('正解率： ', acc_score)
        print()

        f1_score = sklearn.metrics.f1_score(y_label, y_preds, average=None)
        print('F値   ： ', f1_score)

        rec_score_avg = sklearn.metrics.recall_score(
            y_label, y_preds, average="macro")
        print('再現率(平均)： ', rec_score_avg)
        pre_score_avg = sklearn.metrics.precision_score(
            y_label, y_preds, average="macro")
        print('適合率(平均)： ', pre_score_avg)
        f1_score_avg = sklearn.metrics.f1_score(
            y_label, y_preds, average="macro")
        print('F値(平均)   ： ', f1_score_avg)
        print('Time :',end-start)


if __name__ == '__main__':
    _main()

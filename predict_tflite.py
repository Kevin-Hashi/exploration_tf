import argparse
import glob
import os
import re

import numpy as np
import tensorflow as tf
from keras.preprocessing.image import img_to_array, load_img
from keras.utils import np_utils
from tqdm import tqdm

parser=argparse.ArgumentParser()
parser.add_argument('-a','--all',action='store_true')
args=parser.parse_args()


def list_pictures(directory, ext='jpg|jpeg|bmp|png|ppm'):
    return [os.path.join(root, f)
            for root, _, files in os.walk(directory) for f in files
            if re.match(r'([\w]+\.(?:' + ext + '))', f.lower())]

X = []
Y = []

for picture in list_pictures('./test_train/test/0_gu'):
    img = img_to_array(load_img(picture, target_size=(300,300)))
    X.append(img)
    Y.append(0)

for picture in list_pictures('./test_train/test/1_tyoki'):
    img = img_to_array(load_img(picture, target_size=(300,300)))
    X.append(img)
    Y.append(1)

for picture in list_pictures('./test_train/test/2_pa'):
    img = img_to_array(load_img(picture, target_size=(300,300)))
    X.append(img)
    Y.append(2)


x_test= np.asarray(X)
y_test = np.asarray(Y)

x_test=x_test.astype(np.float32)/255

def predict(i,input_details):
    input_data=x_test.astype(np.float32)[i:i+1]
    interpreter.set_tensor(input_details[0]['index'],input_data)
    interpreter.invoke()
    return interpreter.get_tensor(output_details[0]['index'])[0]

tfliteFiles=[i for i in glob.glob('**/*.tflite',recursive=True) if i.split('\\')[0]=="train" or i.split('\\')[0]=="train2"] if args.all else ['']

for tflitefile in tfliteFiles:

    interpreter=tf.lite.Interpreter(model_path=tflitefile)
    interpreter.allocate_tensors()

    input_details=interpreter.get_input_details()
    output_details=interpreter.get_output_details()

    output_data=np.array([predict(i,input_details) for i in tqdm(range(len(y_test)))])
    y_pred=np.argmax(output_data,axis=1)
    #sum([yt==yp for yt,yp in zip(y_test,y_pred)])/len(y_test)
    #for yt,yp in zip(y_test,y_pred):
    #    print(yt,yp)
    print("正解数:",sum([yt==yp for yt,yp in zip(y_test,y_pred)]))
    print(sum([yt==yp for yt,yp in zip(y_test,y_pred)])/len(y_test))
